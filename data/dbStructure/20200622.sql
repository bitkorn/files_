create table file
(
    file_uuid        uuid                                      not null
        constraint file_pk
            primary key,
    file_label       varchar(200)                              not null,
    file_desc        text        default ''::text              not null,
    file_filename    varchar(200)                              not null,
    file_extension   varchar(10)                               not null,
    file_mimetype    varchar(50)                               not null,
    file_time_create timestamp   default CURRENT_TIMESTAMP     not null,
    file_time_update timestamp,
    file_folderbrand varchar(40) default ''::character varying not null
);

comment on column file.file_filename is 'auto generated from file_label plus salt';

alter table file
    owner to postgres;

create table file_supplier_rel
(
    file_supplier_rel_uuid uuid not null
        constraint file_supplier_rel_pk
            primary key,
    file_uuid              uuid not null
        constraint file_supplier_rel_file_file_uuid_fk
            references file,
    supplier_uuid          uuid not null
        constraint file_supplier_rel_supplier_supplier_uuid_fk
            references supplier
);

alter table file_supplier_rel
    owner to postgres;

