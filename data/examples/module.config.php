<?php

namespace Bitkorn\Files;

use Bitkorn\Files\Factory\Controller\Html\FilesControllerFactory;
use Controller\Html\FilesController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router'             => [
        'routes' => [
            'bitkorn_files_fileform' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/bk-file-form',
                    'defaults' => [
                        'controller' => FilesController::class,
                        'action'     => 'upload',
                    ],
                ],
            ],
            'bitkorn_files_download' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/bk-file-download[/:file_uuid]',
                    'constraints' => [
                        'file_uuid' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FilesController::class,
                        'action'     => 'download',
                    ],
                ],
            ],
        ],
    ],
    'controllers_demo'        => [
        'factories'  => [
            FilesController::class => FilesControllerFactory::class,
        ],
    ],
];
