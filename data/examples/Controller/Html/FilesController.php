<?php

namespace Controller\Html;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Form\FileForm;
use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Controller\AbstractHtmlController;
use Laminas\Form\FormInterface;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;
use Laminas\View\Model\ViewModel;

class FilesController extends AbstractHtmlController
{
    protected FileForm $fileForm;
    protected FileService $fileService;

    public function setFileForm(FileForm $fileForm): void
    {
        $this->fileForm = $fileForm;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    /**
     * Demo
     * @return ViewModel
     */
    public function uploadActionDemo(): ViewModel
    {
        $viewModel = new ViewModel();
        $request = $this->getRequest();
        if (!$request instanceof Request) {
            return $viewModel;
        }
        $this->fileForm->init();
        if ($request->isPost()) {
            $post = ArrayUtils::merge($request->getPost()->toArray(), $request->getFiles()->toArray());
            $this->fileForm->setData($post);
            if ($this->fileForm->isValid()) {
                $formData = $this->fileForm->getData(FormInterface::VALUES_AS_ARRAY)['file_fieldset'];
                $fileEntity = new FileEntity();
                if (!$fileEntity->exchangeArrayFromDatabase($formData)) {
                    throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() can not exchange array for entity');
                }
                if (!empty($fileUuid = $this->fileService->handleFileUpload($fileEntity))) {
                    $viewModel->setVariable('fileUuid', $fileUuid);
                }
            }
        }
        $viewModel->setVariable('fileForm', $this->fileForm);
        return $viewModel;
    }

    /**
     * Demo
     * @return ViewModel
     */
    public function downloadActionDemo(): ViewModel
    {
        $viewModel = new ViewModel();
        $fileUuid = $this->params('file_uuid');
        if (!(new Uuid())->isValid($fileUuid)) {
            $viewModel->setVariable('message', 'invalid file uuid');
            return $viewModel;
        }
        $this->fileService->outputFileAsAttachment($fileUuid);

        return $viewModel;
    }
}
