<?php

namespace Bitkorn\Files;

use Bitkorn\Files\Command\FreeformatterMimeTypesParseCommand;
use Bitkorn\Files\Controller\Ajax\FilesListsAjaxController;
use Bitkorn\Files\Factory\Command\FreeformatterMimeTypesParseCommandFactory;
use Bitkorn\Files\Factory\Controller\Ajax\FilesListsAjaxControllerFactory;
use Bitkorn\Files\Factory\Form\FileFieldsetFactory;
use Bitkorn\Files\Factory\Form\FileFormFactory;
use Bitkorn\Files\Factory\Service\FileCategoryServiceFactory;
use Bitkorn\Files\Factory\Table\FileMimeTableFactory;
use Bitkorn\Files\Factory\Table\ViewFileCategoryBrandTableFactory;
use Bitkorn\Files\Factory\Table\ViewFileCategoryTableFactory;
use Bitkorn\Files\Form\FileFieldset;
use Bitkorn\Files\Form\FileForm;
use Bitkorn\Files\Service\FileCategoryService;
use Bitkorn\Files\Table\FileMimeTable;
use Bitkorn\Files\Table\ViewFileCategoryBrandTable;
use Bitkorn\Files\Table\ViewFileCategoryTable;
use Laminas\Router\Http\Segment;

return [
    'router'                             => [
        'routes' => [
            /*
             * AJAX
             */
            'bitkorn_files_ajax_lists_categoriesbrandassoc' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/bitkorn-files-categories-brand-assoc/:brand',
                    'constraints' => [
                        'brand' => '[0-9A-Za-z_]+',
                    ],
                    'defaults'    => [
                        'controller' => FilesListsAjaxController::class,
                        'action'     => 'categoriesBrandIdAssoc'
                    ],
                ],
            ],
        ]
    ],
    'controllers'                        => [
        'factories' => [
            FilesListsAjaxController::class => FilesListsAjaxControllerFactory::class,
        ]
    ],
    'service_manager'                    => [
        'factories'  => [
            // service
            Service\FileService::class                => Factory\Service\FileServiceFactory::class,
            FileCategoryService::class                => FileCategoryServiceFactory::class,
            // table
            Table\FileTable::class                    => Factory\Table\FileTableFactory::class,
            FileMimeTable::class                      => FileMimeTableFactory::class,
            ViewFileCategoryTable::class              => ViewFileCategoryTableFactory::class,
            ViewFileCategoryBrandTable::class         => ViewFileCategoryBrandTableFactory::class,
            // fieldset
            FileFieldset::class                       => FileFieldsetFactory::class,
            // form
            FileForm::class                           => FileFormFactory::class,
            // command
            FreeformatterMimeTypesParseCommand::class => FreeformatterMimeTypesParseCommandFactory::class,
        ],
        'invokables' => [],
    ],
    'laminas-cli'                        => [
        'commands' => [
            'file:parse-freeformatter' => FreeformatterMimeTypesParseCommand::class,
        ],
    ],
    'view_helper_config'                 => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'view_manager'                       => [
        'template_map'        => [],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'bitkorn_files_category_brands'      => [
        /**
         * Each module can have its own category brands for table 'file_category_rel'.
         * The key is known in the module.
         */
        'bitkorn_files'         => 'example_brand',
        'bitkorn_files_special' => 'example_brand_special',
    ],
    'bitkorn_files_category_ids_exclude' => [
        /**
         * The key is the value from one entry in module.config.php[bitkorn_files_category_brands]
         */
        'example_brand' => [1, 2, 3],
    ],
    'bitkorn_files'                      => [
        'files_upload_path' => __DIR__ . '/../../../../data/files/upload',
        // https://docs.laminas.dev/laminas-validator/validators/file/size/#supported-options
        'files_size_min'    => 0,
        'files_size_max'    => '10MB',
        // https://www.iana.org/assignments/media-types/media-types.xhtml
        // https://filext.com/faq/office_mime_types.html
        'files_mimetypes'   => ['image/jpg', 'image/jpeg', 'image/png', 'application/pdf', 'text/plain', 'text/csv', 'text/rtf', 'text/vcard', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
    ],
];
