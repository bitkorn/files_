<?php

namespace Bitkorn\Files\Form;

use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\InputFilter\InputFilterProviderInterface;

class FileForm extends AbstractForm implements InputFilterProviderInterface
{
    protected FileFieldset $fileFieldset;

    public function setFileFieldset(FileFieldset $fileFieldset): void
    {
        $this->fileFieldset = $fileFieldset;
    }

    public function getFileFieldset(): FileFieldset
    {
        return $this->fileFieldset;
    }

    public function init()
    {
        $this->add($this->fileFieldset);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'los',
            ],
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return $this->fileFieldset->getInputFilterSpecification();
    }
}
