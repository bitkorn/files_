<?php

namespace Bitkorn\Files\Form;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Laminas\Form\Element\File;
use Laminas\Form\Element\Text;
use Laminas\Form\Fieldset;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Digits;
use Laminas\Validator\File\MimeType;
use Laminas\Validator\File\Size;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class FileFieldset extends Fieldset implements InputFilterProviderInterface
{
    protected bool $primaryKeyAvailable = false;
    protected bool $fileInputAvailable = false;

    /**
     * https://www.iana.org/assignments/media-types/media-types.xhtml
     * @var array|string[]
     */
    protected array $mimeTypes = ['image/jpg', 'image/png', 'application/pdf'];

    protected string $fileSizeMin = '0';
    protected string $fileSizeMax = '1MB';

    public function setPrimaryKeyAvailable(bool $primaryKeyAvailable): void
    {
        $this->primaryKeyAvailable = $primaryKeyAvailable;
    }

    public function setFileInputAvailable(bool $fileInputAvailable): void
    {
        $this->fileInputAvailable = $fileInputAvailable;
    }

    /**
     * https://docs.laminas.dev/laminas-validator/validators/file/mime-type/#supported-options
     *
     * @param array $mimeTypes
     */
    public function setMimeTypes(array $mimeTypes): void
    {
        $this->mimeTypes = $mimeTypes;
    }

    /**
     * https://docs.laminas.dev/laminas-validator/validators/file/size/#supported-options
     *
     * @param string $fileSizeMin
     */
    public function setFileSizeMin(string $fileSizeMin): void
    {
        $this->fileSizeMin = $fileSizeMin;
    }

    /**
     * https://docs.laminas.dev/laminas-validator/validators/file/size/#supported-options
     *
     * @param string $fileSizeMax
     */
    public function setFileSizeMax(string $fileSizeMax): void
    {
        $this->fileSizeMax = $fileSizeMax;
    }

    public function init()
    {
        $this->setName('file_fieldset');

        if ($this->primaryKeyAvailable) {
            $this->add([
                'name'    => 'file_uuid',
                'type'    => Text::class,
                'options' => [
                    'label' => 'UUID',
                ],
            ]);
        }

        $this->add([
            'name'    => 'file_label',
            'type'    => Text::class,
            'options' => [
                'label' => 'Label',
            ],
        ]);

        $this->add([
            'name'    => 'file_desc',
            'type'    => Text::class,
            'options' => [
                'label' => 'desc',
            ],
        ]);

        if ($this->fileInputAvailable) {
            $this->add([
                'name'    => 'file',
                'type'    => File::class,
                'options' => [
                    'label' => 'Datei',
                ],
            ]);
        }

        $this->add([
            'name'    => 'file_folderbrand',
            'type'    => Text::class,
            'options' => [
                'label' => 'module brand',
            ],
        ]);

        $this->add([
            'name'    => 'file_category_id',
            'type'    => Text::class,
            'options' => [
                'label' => 'module brand',
            ],
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];
        /**
         * @todo warum nur hier notwendig (sonst "value is required and can't be empty")? ...und nicht auch in bitkorn/address | bitkorn/bank | bitkorn/contact
         */
        $filter['file_fieldset'] = ['required' => false];

        if ($this->primaryKeyAvailable) {
            $filter['file_uuid'] = [
                'required'   => true,
                'validators' => [['name' => Uuid::class,]]
            ];
        }

        $filter['file_label'] = [
            'required'   => true,
            'filters'    => [
                ['name' => FilterChainStringSanitize::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 180,
                    ]
                ]
            ]
        ];

        $filter['file_desc'] = [
            'required'   => false,
            'filters'    => [
                ['name' => FilterChainStringSanitize::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 60000,
                    ]
                ]
            ]
        ];

        if ($this->fileInputAvailable) {
            $filter['file'] = [
                'required'   => true,
                'filters'    => [
                ],
                'validators' => [
                    [
                        'name'                   => MimeType::class,
                        'break_chain_on_failure' => true,
                        'options'                => [
                            'enableHeaderCheck' => true,
                            'disableMagicFile'  => true,
                            'magicFile'         => null, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
                            'mimeType'          => $this->mimeTypes,
                            'messages'          => [
                                MimeType::FALSE_TYPE => "Falsches Dateiformat '%type%', nur " . implode(' | ', $this->mimeTypes),
                            ],
                        ]
                    ],
                    [
                        'name'    => Size::class,
                        'options' => [
                            'min'           => $this->fileSizeMin,
                            'max'           => $this->fileSizeMax,
                            'useByteString' => true, // Use byte string?
                            'messages'      => [
                                Size::TOO_BIG   => "Die Datei ist mit %size% zu groß, max=%max%!",
                                Size::TOO_SMALL => "Die Datei ist mit %size% zu klein, max=%min%!",
                            ],
                        ],
                    ],
                ],
            ];
        }

        $filter['file_folderbrand'] = [
            'required'   => false,
            'filters'    => [
                ['name' => FilterChainStringSanitize::class]
            ],
            'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 20,
                    ]
                ]
            ]
        ];

        $filter['file_category_id'] = [
            'required'   => false,
            'filters'    => [
                ['name' => FilterChainStringSanitize::class]
            ],
            'validators' => [
                [
                    'name' => Digits::class,
                ]
            ]
        ];

        return $filter;
    }
}
