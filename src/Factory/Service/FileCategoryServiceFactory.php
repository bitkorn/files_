<?php

namespace Bitkorn\Files\Factory\Service;

use Bitkorn\Files\Service\FileCategoryService;
use Bitkorn\Files\Table\ViewFileCategoryBrandTable;
use Bitkorn\Files\Table\ViewFileCategoryTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FileCategoryServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FileCategoryService();
        $service->setLogger($container->get('logger'));
        $service->setViewFileCategoryTable($container->get(ViewFileCategoryTable::class));
        $service->setViewFileCategoryBrandTable($container->get(ViewFileCategoryBrandTable::class));
        return $service;
    }
}
