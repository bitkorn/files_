<?php

namespace Bitkorn\Files\Factory\Service;

use Bitkorn\Files\Service\FileService;
use Bitkorn\Files\Table\FileMimeTable;
use Bitkorn\Files\Table\FileTable;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FileServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FileService();
        $service->setLogger($container->get('logger'));
        $service->setFileUploadPath($container->get('config')['bitkorn_files']['files_upload_path']);
        $service->setFileTable($container->get(FileTable::class));
        $service->setFileMimeTable($container->get(FileMimeTable::class));
        $service->setDbAdapter($container->get('dbDefault'));
        $service->setFolderTool($container->get(FolderTool::class));
        return $service;
    }
}
