<?php

namespace Bitkorn\Files\Factory\Controller\Ajax;

use Bitkorn\Files\Controller\Ajax\FilesListsAjaxController;
use Bitkorn\Files\Service\FileCategoryService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FilesListsAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FilesListsAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFileCategoryService($container->get(FileCategoryService::class));
        $controller->setFileCategoryIdsBrandExclude($container->get('config')['bitkorn_files_category_ids_exclude']);
        return $controller;
    }
}
