<?php

namespace Bitkorn\Files\Factory\Controller\Html;

use Bitkorn\Files\Form\FileForm;
use Bitkorn\Files\Service\FileService;
use Controller\Html\FilesController;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FilesControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FilesController();
        $controller->setLogger($container->get('logger'));
        $controller->setFileForm($container->get(FileForm::class));
        $controller->setFileService($container->get(FileService::class));
        return $controller;
    }
}
