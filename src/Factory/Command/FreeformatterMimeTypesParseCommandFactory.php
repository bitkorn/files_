<?php

namespace Bitkorn\Files\Factory\Command;

use Bitkorn\Files\Command\FreeformatterMimeTypesParseCommand;
use Bitkorn\Files\Table\FileMimeTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FreeformatterMimeTypesParseCommandFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $command = new FreeformatterMimeTypesParseCommand();
        $command->setLogger($container->get('logger'));
        $command->setFileMimeTable($container->get(FileMimeTable::class));
        return $command;
    }
}
