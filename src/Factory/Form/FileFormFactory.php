<?php

namespace Bitkorn\Files\Factory\Form;

use Bitkorn\Files\Form\FileFieldset;
use Bitkorn\Files\Form\FileForm;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FileFormFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$form = new FileForm();
        $form->setFileFieldset($container->get(FileFieldset::class));
//        $form->bi
		return $form;
	}
}
