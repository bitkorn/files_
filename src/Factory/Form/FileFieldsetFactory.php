<?php


namespace Bitkorn\Files\Factory\Form;


use Bitkorn\Files\Form\FileFieldset;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FileFieldsetFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $fieldset = new FileFieldset();
        $config = $container->get('config')['bitkorn_files'];
        $fieldset->setFileSizeMin($config['files_size_min']);
        $fieldset->setFileSizeMax($config['files_size_max']);
        $fieldset->setMimeTypes($config['files_mimetypes']);
        $fieldset->init();
        return $fieldset;
    }
}
