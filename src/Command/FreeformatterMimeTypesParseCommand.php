<?php

namespace Bitkorn\Files\Command;

use Bitkorn\Files\Table\FileMimeTable;
use Laminas\Log\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FreeformatterMimeTypesParseCommand extends Command
{
    protected Logger $logger;
    protected string $mimetypesHtmlPath = __DIR__ . '/../../data/freeformatter-mimetypes-list.html';
    protected FileMimeTable $fileMimeTable;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setFileMimeTable(FileMimeTable $fileMimeTable): void
    {
        $this->fileMimeTable = $fileMimeTable;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>parse...</info>');
        if (!file_exists($this->mimetypesHtmlPath)) {
            $output->writeln('<error>Die MIME Types Liste Datei existiert nicht: ' . $this->mimetypesHtmlPath . '</error>');
            return -1;
        }
        try {
            $xml = new \SimpleXMLElement(file_get_contents($this->mimetypesHtmlPath));
        } catch (\Exception $exception) {
            $output->writeln('<error>Fehler beim Einlesen der HTML Datei:</error>');
            $output->writeln('<error>' . $exception->getMessage() . '</error>');
            return -1;
        }
        if (!$this->fileMimeTable->truncateTable()) {
            $output->writeln('<error>purge table not success</error>');
        }
        foreach ($xml->tr as $tr) {
            if (!str_starts_with($tr->td[2], '.')) {
                continue;
            }
            $output->writeln('<comment>label: ' . $tr->td[0] . '; MIME: ' . $tr->td[1] . '; Ext.: ' . $tr->td[2] . '; desc: ' . $tr->td[3]->a['href'] . '</comment>');
            $output->writeln('<info>file_mime_id: ' . $this->fileMimeTable->insertFileMime($tr->td[0], $tr->td[1], substr($tr->td[2], 1), $tr->td[3]->a['href']) . '</info>');
        }
        $output->writeln('<info>...feddich :)</info>');
        return 1;
    }
}
