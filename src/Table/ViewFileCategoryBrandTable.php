<?php

namespace Bitkorn\Files\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class ViewFileCategoryBrandTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_file_category_brand';

    /**
     * @param string $brand From module.config.php key 'bitkorn_files_category_brands'.
     * @return array
     */
    public function getViewFileCategoriesBrand(string $brand, array $excludeCategoryIds = []): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_category_rel_brand' => $brand]);
            $select->where->notIn('file_category_id', $excludeCategoryIds);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $brand One from module.config.php key 'bitkorn_files_category_brands'.
     * @param array $excludeCategoryIds
     * @return array
     */
    public function getViewFileCategoriesBrandIdAssoc(string $brand, array $excludeCategoryIds = []): array
    {
        $cats = $this->getViewFileCategoriesBrand($brand, $excludeCategoryIds);
        $assoc = [];
        foreach ($cats as $cat) {
            $assoc[$cat['file_category_id']] = $cat['file_category_label'];
        }
        return $assoc;
    }
}
