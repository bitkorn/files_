<?php

namespace Bitkorn\Files\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class FileMimeTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'file_mime';

    /**
     * @param string $fileMimeId
     * @return array
     */
    public function getFileMime(string $fileMimeId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_mime_id' => $fileMimeId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $fileMimeMime The MIME type like on https://www.iana.org/assignments/media-types/media-types.xhtml
     * @return array
     */
    public function getFileMimeByMime(string $fileMimeMime): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_mime_mime' => $fileMimeMime]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $fileMimeMime
     * @return string The file extension without the dot or an empty string.
     */
    public function getFileMimeExtensionByMime(string $fileMimeMime): string
    {
        if (empty($fileMime = $this->getFileMimeByMime($fileMimeMime))) {
            return '';
        }
        return $fileMime['file_mime_ext'];
    }

    /**
     * @param string $label
     * @param string $mime
     * @param string $extension
     * @param string $desc
     * @return int The last inserted `file_mime_id` or -1.
     */
    public function insertFileMime(string $label, string $mime, string $extension, string $desc): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'file_mime_label' => $label,
                'file_mime_mime'  => $mime,
                'file_mime_ext'   => $extension,
                'file_mime_desc'  => $desc,
            ]);
            if ($this->insertWith($insert) == 1) {
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.file_mime_file_mime_id_seq');
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
