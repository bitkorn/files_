<?php

namespace Bitkorn\Files\Table;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;

class FileTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'file';

    /**
     * @param string $fileUuid
     * @return array With additional key 'file_time_create_unix';
     */
    public function getFile(string $fileUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->columns([
                Select::SQL_STAR,
                'file_time_create_unix' => new Expression("date_part('epoch'::text, date_trunc('second'::text, file_time_create)::timestamp(0) without time zone)")
            ]);
            $select->where(['file_uuid' => $fileUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFile(FileEntity $fileEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'file_uuid'        => $uuid,
                'file_label'       => $fileEntity->getFileLabel(),
                'file_desc'        => $fileEntity->getFileDesc(),
                'file_filename'    => $fileEntity->getFilename(),
                'file_extension'   => $fileEntity->getFileExtension(),
                'file_mimetype'    => $fileEntity->getFileMimetype(),
                'file_folderbrand' => $fileEntity->getFileFolderbrand(),
                'file_category_id' => $fileEntity->getFileCategoryId() ?: null,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $fileUuid
     * @return int
     */
    public function deleteFile(string $fileUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['file_uuid' => $fileUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * Updates only the three fields file_label, file_desc and file_category_id.
     * Additional it sets the file_time_update timestamp.
     *
     * @param string $fileUuid
     * @param FileEntity $fileEntity
     * @return int
     */
    public function updateFile(string $fileUuid, FileEntity $fileEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'file_label'       => $fileEntity->getFileLabel(),
                'file_desc'        => $fileEntity->getFileDesc(),
                'file_time_update' => new Expression('CURRENT_TIMESTAMP'),
                'file_category_id' => $fileEntity->getFileCategoryId() ?: null,
            ]);
            $update->where(['file_uuid' => $fileUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * It UPDATEs not the field 'file_time_update' with `CURRENT_TIMESTAMP`!
     *
     * @param FileEntity $fileEntity
     * @return int
     */
    public function updateFileIncludeFilename(FileEntity $fileEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'file_label'    => $fileEntity->getFileLabel(),
                'file_desc'     => $fileEntity->getFileDesc(),
                'file_filename' => $fileEntity->getFilename(),
            ]);
            $update->where(['file_uuid' => $fileEntity->getFileUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param Select $selectFileUuids A Select object, that only selects file_uuid's.
     * @return array
     */
    public function getFiles(Select $selectFileUuids): array
    {
        $select = $this->sql->select();
        try {
            $select->where->in('file_uuid', $selectFileUuids);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
