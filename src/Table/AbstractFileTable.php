<?php

namespace Bitkorn\Files\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;

class AbstractFileTable extends AbstractLibTable
{
    protected array $filesColumns = ['file_label', 'file_desc', 'file_filename', 'file_extension', 'file_mimetype', 'file_time_create', 'file_time_update', 'file_folderbrand', 'file_category_id'];

    protected FileTable $fileTable;

    public function setFileTable(FileTable $fileTable): void
    {
        $this->fileTable = $fileTable;
    }
}
