<?php

namespace Bitkorn\Files\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class ViewFileCategoryTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'view_file_category';

    /**
     * @param string $viewFileCategoryUuid
     * @return array All categories from 'view_file_category'
     */
    public function getViewFileCategory(string $viewFileCategoryUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['view_file_category_uuid' => $viewFileCategoryUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
