<?php

namespace Bitkorn\Files\Controller\Ajax;

use Bitkorn\Files\Service\FileCategoryService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;

class FilesListsAjaxController extends AbstractUserController
{
    protected FileCategoryService $fileCategoryService;
    protected array $fileCategoryIdsBrandExclude = [];

    public function setFileCategoryService(FileCategoryService $fileCategoryService): void
    {
        $this->fileCategoryService = $fileCategoryService;
    }

    public function setFileCategoryIdsBrandExclude(array $fileCategoryIdsBrandExclude): void
    {
        $this->fileCategoryIdsBrandExclude = $fileCategoryIdsBrandExclude;
    }

    /**
     * @return JsonModel
     */
    public function categoriesBrandIdAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $brand = filter_var($this->params('brand'), FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        $idsExclude = (!empty($this->fileCategoryIdsBrandExclude) && !empty($this->fileCategoryIdsBrandExclude[$brand])) ? $this->fileCategoryIdsBrandExclude[$brand] : [];
        $jsonModel->setObj($this->fileCategoryService->getFileCategoriesBrandIdAssoc($brand, $idsExclude));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
