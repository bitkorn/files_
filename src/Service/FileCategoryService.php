<?php

namespace Bitkorn\Files\Service;

use Bitkorn\Files\Table\ViewFileCategoryBrandTable;
use Bitkorn\Files\Table\ViewFileCategoryTable;
use Bitkorn\Trinket\Service\AbstractService;

class FileCategoryService extends AbstractService
{
    protected ViewFileCategoryTable $viewFileCategoryTable;
    protected ViewFileCategoryBrandTable $viewFileCategoryBrandTable;

    public function setViewFileCategoryTable(ViewFileCategoryTable $viewFileCategoryTable): void
    {
        $this->viewFileCategoryTable = $viewFileCategoryTable;
    }

    public function setViewFileCategoryBrandTable(ViewFileCategoryBrandTable $viewFileCategoryBrandTable): void
    {
        $this->viewFileCategoryBrandTable = $viewFileCategoryBrandTable;
    }

    /**
     * @param string $brand From module.config.php key 'bitkorn_files_category_brands'.
     * @param array $excludeCategoryIds
     * @return array
     */
    public function getFileCategoriesBrandIdAssoc(string $brand, array $excludeCategoryIds = []): array
    {
        return $this->viewFileCategoryBrandTable->getViewFileCategoriesBrandIdAssoc($brand, $excludeCategoryIds);
    }
}
