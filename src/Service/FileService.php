<?php

namespace Bitkorn\Files\Service;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Table\FileMimeTable;
use Bitkorn\Files\Table\FileTable;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\Trinket\Tools\File\FileTool;
use Bitkorn\Trinket\Tools\Filesystem\FilenameTool;
use Bitkorn\Trinket\Tools\Render\RenderTool;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Stdlib\ArrayUtils;

class FileService extends AbstractService implements AdapterAwareInterface
{
    protected string $fileUploadPath;
    protected FileTable $fileTable;
    protected FileMimeTable $fileMimeTable;
    protected Adapter $adapter;
    protected FolderTool $folderTool;

    public function setFileUploadPath(string $fileUploadPath): void
    {
        $this->fileUploadPath = realpath($fileUploadPath);
    }

    public function setFileTable(FileTable $fileTable): void
    {
        $this->fileTable = $fileTable;
    }

    public function setFileMimeTable(FileMimeTable $fileMimeTable): void
    {
        $this->fileMimeTable = $fileMimeTable;
    }

    public function setDbAdapter(Adapter $adapter): AdapterAwareInterface
    {
        $this->adapter = $adapter;
        return $this;
    }

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    /**
     * @param string $path
     * @return array [0 => MIMEType, 1 => fileExtension] or an empty array.
     */
    public function getFileinfo(string $path): array
    {
        if (!file_exists($path)
            || empty($mimeType = FileTool::getMimeTypeFromFileinfo($path))
            || empty($ext = $this->fileMimeTable->getFileMimeExtensionByMime($mimeType))
        ) {
            return [];
        }
        return [$mimeType, $ext];
    }

    /**
     * INSERT INTO TABLE file ...with a FileEntity.
     *
     * @param FileEntity $fileEntity
     * @return string
     */
    public function insertFile(FileEntity $fileEntity): string
    {
        return $this->fileTable->insertFile($fileEntity);
    }

    public function updateFile(string $fileUuid, FileEntity $fileEntity): bool
    {
        return $this->fileTable->updateFile($fileUuid, $fileEntity) >= 0;
    }

    public function deleteFile(string $fileUuid): bool
    {
        $fileX = $this->getFileForOutput($fileUuid);
        if (empty($fileX) || empty($fileX['fqfn'])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() No file for file_uuid ' . $fileUuid);
        }
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        if ($this->fileTable->deleteFile($fileUuid) < 1 || (file_exists($fileX['fqfn']) && !unlink($fileX['fqfn']))) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    /**
     * Upload and insert (db table) a file.
     * It uses transaction: If something goes wrong, no database entry is made.
     *
     * @param FileEntity $fileEntity Storage 'file' (html file input) & '' are required, 'file_filename' will be created.
     * @param string $folderBrand 'folderBrand' in the FileEntity parameter has priority (if both not empty the value from FileEntity is used).
     * @return string File UUID or an empty string.
     */
    public function handleFileUpload(FileEntity $fileEntity, string $folderBrand = ''): string
    {
        if (!file_exists($this->fileUploadPath)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Folder "' . $this->fileUploadPath . '" does not exist.');
        }
        if (
            empty($fileInfo = $this->getFileinfo($fileEntity->getFileinputTmpName()))
            || empty($fileName = FilenameTool::computeSeoFriendlyFilename($fileEntity->getFileLabel()))
        ) {
            return '';
        }
        $folderBrand = $fileEntity->getFileFolderbrand() ?: $folderBrand;
        $fileEntity->setFilename($fileName); // first filename
        $fileEntity->setFileExtension($fileInfo[1]);
        $fileEntity->setFileMimetype($fileInfo[0]);
        $fileEntity->setFileFolderbrand($folderBrand);

        $conn = $this->beginTransactionAdapter($this->adapter);

        if (empty($fileUuid = $this->insertFile($fileEntity)) || empty($file = $this->fileTable->getFile($fileUuid))
            || empty($timeCreate = intval($file['file_time_create_unix']))) {
            $conn->rollback();
            return '';
        }
        $fileEntity->setFileUuid($file['file_uuid']);
        $fileEntity->setFileTimeCreate($file['file_time_create']); // for expandFilename()
        $fileName = $fileEntity->expandFilename(); // final filename
        if ($this->fileTable->updateFileIncludeFilename($fileEntity) < 0) {
            $conn->rollback();
            return '';
        }
        $path = $this->folderTool->computeBrandDateFolder($this->fileUploadPath, $folderBrand, date('Y', $timeCreate), date('m', $timeCreate));
        $fqfn = $path . DIRECTORY_SEPARATOR . $fileName . '.' . $fileInfo[1];
        if (!move_uploaded_file($fileEntity->getFileinputTmpName(), $fqfn)) {
            $conn->rollback();
            return '';
        }
        $conn->commit();
        return $fileUuid;
    }

    /**
     * Copy and insert (db table) a file.
     * It uses no transaction.
     *
     * @param string $filePath Must include the file extension (e.g. pdf or odt).
     * @param string $fileLabel
     * @param string $fileDesc
     * @param string $folderBrand
     * @param int $categoryId
     * @return string The new file_uuid OR empty string. If it is an empty string then $this->message is available.
     */
    public function handleFileCopy(string $filePath, string $fileLabel, string $fileDesc, string $folderBrand = '', int $categoryId = 0): string
    {
        if (!file_exists($this->fileUploadPath)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Folder "' . $this->fileUploadPath . '" does not exist.');
        }
        if (!file_exists($filePath)) {
            $this->message = __CLASS__ . '()->' . __FUNCTION__ . '() file does not exist - path: ' . $filePath;
            return '';
        }
        $fileName = FilenameTool::computeSeoFriendlyFilename($fileLabel) ?: FileTool::getFilenameFromFilepath($filePath);
        if (empty($fileName)) {
            $this->message = __CLASS__ . '()->' . __FUNCTION__ . '() Not possible to create a filename';
            return '';
        }
        if (empty($mimeType = FileTool::getMimeTypeFromFileinfo($filePath))) {
            $this->message = __CLASS__ . '()->' . __FUNCTION__ . '() MIME type from file_info is empty';
            return '';
        }
        $ext = strtolower(FileTool::getExtensionFromFilepath($filePath));
        $fileEntity = new FileEntity();
        $fileEntity->setFileLabel($fileLabel);
        $fileEntity->setFileDesc($fileDesc);
        $fileEntity->setFilename($fileName); // first filename
        $fileEntity->setFileExtension($ext);
        $fileEntity->setFileMimetype($mimeType);
        $fileEntity->setFileFolderbrand($folderBrand);
        $fileEntity->setFileCategoryId($categoryId);
        if (empty($fileUuid = $this->insertFile($fileEntity)) || empty($file = $this->fileTable->getFile($fileUuid))
            || empty($timeCreateUnix = intval($file['file_time_create_unix']))) {
            $this->message = __CLASS__ . '()->' . __FUNCTION__ . '() Error while insert file.';
            return '';
        }
        $fileEntity->setFileUuid($file['file_uuid']);
        $fileEntity->setFileTimeCreate($file['file_time_create']); // for expandFilename()
        $fileName = $fileEntity->expandFilename(); // final filename
        if ($this->fileTable->updateFileIncludeFilename($fileEntity) < 0) {
            $this->message = __CLASS__ . '()->' . __FUNCTION__ . '() Error while update filename.';
            return '';
        }
        $path = $this->folderTool->computeBrandDateFolder($this->fileUploadPath, $folderBrand, date('Y', $timeCreateUnix), date('m', $timeCreateUnix));
        if (!is_writable($path)) {
            $this->message = 'Directory ' . $path . ' is not writable.';
            return '';
        }
        $fqfn = $path . DIRECTORY_SEPARATOR . $fileName . '.' . $ext;
        if (!copy($filePath, $fqfn)) {
            $this->message = __CLASS__ . '()->' . __FUNCTION__ . '() Error while copy file into ' . $path;
            return '';
        }
        return $fileUuid;
    }

    /**
     * @param string $fileUuid
     * @return array Fields from db plus 'file_filename_complete' & 'file_filename_label' & 'fqfn' & 'fqdn'.
     */
    public function getFileForOutput(string $fileUuid): array
    {
        if (empty($file = $this->fileTable->getFile($fileUuid)) || empty($timeCreate = intval($file['file_time_create_unix']))) {
            return [];
        }
        $file['file_filename_label'] = str_replace([' ', "\n", "\r", "\t", "\v", "\x00"], '_', $file['file_label']) . '.' . $file['file_extension'];
        $file['file_filename_complete'] = $file['file_filename'] . '.' . $file['file_extension'];
        $fqdn = $this->folderTool->getBrandDateFolder($this->fileUploadPath, $file['file_folderbrand'], date('Y', $timeCreate), date('m', $timeCreate));
        $fqfn = $fqdn . DIRECTORY_SEPARATOR . $file['file_filename_complete'];
        return ArrayUtils::merge($file, ['fqfn' => $fqfn, 'fqdn' => $fqdn]);
    }

    public function outputFileAsAttachment(string $fileUuid): void
    {
        if (empty($ffout = $this->getFileForOutput($fileUuid)) || empty($ffout['file_filename_complete']) || empty($ffout['file_mimetype'])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Can not find a file for UUID ' . $fileUuid);
        }
        if (!file_exists($ffout['fqfn'])) {
            $this->message = 'File ' . $ffout['file_label'] . ' not found; file_uuid: ' . $fileUuid;
            return;
        }
        RenderTool::outputAttachment($ffout['fqfn'], $ffout['file_filename_complete'], $ffout['file_mimetype']);
    }
}
