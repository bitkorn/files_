<?php

namespace Bitkorn\Files\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class FileEntity extends AbstractEntity
{
    public array $mapping = [
        'file_uuid'             => 'file_uuid',
        'file_label'            => 'file_label',
        'file_desc'             => 'file_desc',
        'file'                  => 'file', // <input type="file (name, type, tmp_name, error, size)
        'file_filename'         => 'file_filename',
        'file_extension'        => 'file_extension',
        'file_mimetype'         => 'file_mimetype',
        'file_time_create'      => 'file_time_create',
        'file_time_create_unix' => 'file_time_create_unix', // computed
        'file_time_update'      => 'file_time_update',
        'file_folderbrand'      => 'file_folderbrand',
        'file_category_id'      => 'file_category_id',
    ];

    protected $primaryKey = 'file_uuid';

    protected $dbUnusedFields = ['file_time_create', 'file_time_create_unix'];

    /**
     * @return string Filename with 'file_time_create' and 'file_filename'.
     */
    public function expandFilename(): string
    {
        $this->setFilename(str_replace([' ', ':', '.'], ['_', '-', '-'], $this->getFileTimeCreate()) . '_' . $this->getFilename());
        return $this->getFilename();
    }

    public function getStorage(): array
    {
        $storage = $this->storage;
        unset($storage['file']);
        return $storage;
    }

    public function getFileUuid(): string
    {
        if (!isset($this->storage['file_uuid'])) {
            return '';
        }
        return $this->storage['file_uuid'];
    }

    public function setFileUuid(string $fileUuid): void
    {
        $this->storage['file_uuid'] = $fileUuid;
    }

    public function getFileLabel(): string
    {
        if (!isset($this->storage['file_label'])) {
            return '';
        }
        return $this->storage['file_label'];
    }

    public function setFileLabel(string $fileLabel): void
    {
        $this->storage['file_label'] = $fileLabel;
    }

    public function getFileDesc(): string
    {
        if (!isset($this->storage['file_desc'])) {
            return '';
        }
        return $this->storage['file_desc'];
    }

    public function setFileDesc(string $fileDesc): void
    {
        $this->storage['file_desc'] = $fileDesc;
    }

    public function getFilename(): string
    {
        if (!isset($this->storage['file_filename'])) {
            return '';
        }
        return $this->storage['file_filename'];
    }

    public function setFilename(string $filename): void
    {
        $this->storage['file_filename'] = $filename;
    }

    public function getFileExtension(): string
    {
        if (!isset($this->storage['file_extension'])) {
            return '';
        }
        return $this->storage['file_extension'];
    }

    public function setFileExtension(string $ext): void
    {
        $this->storage['file_extension'] = $ext;
    }

    public function getFileMimetype(): string
    {
        if (!isset($this->storage['file_mimetype'])) {
            return '';
        }
        return $this->storage['file_mimetype'];
    }

    public function setFileMimetype(string $mimetype): void
    {
        $this->storage['file_mimetype'] = $mimetype;
    }

    public function getFileTimeCreate(): string
    {
        if (!isset($this->storage['file_time_create'])) {
            return '';
        }
        return $this->storage['file_time_create'];
    }

    public function setFileTimeCreate(string $fileTimeCreate): void
    {
        $this->storage['file_time_create'] = $fileTimeCreate;
    }

    public function getFileTimeCreateUnix(): int
    {
        if (!isset($this->storage['file_time_create_unix'])) {
            return 0;
        }
        return $this->storage['file_time_create_unix'];
    }

    public function setFileTimeCreateUnix(int $fileTimeCreate): void
    {
        $this->storage['file_time_create_unix'] = $fileTimeCreate;
    }

    public function getFileTimeUpdate(): string
    {
        if (!isset($this->storage['file_time_update'])) {
            return '';
        }
        return $this->storage['file_time_update'];
    }

    public function getFileFolderbrand(): string
    {
        if (!isset($this->storage['file_folderbrand'])) {
            return '';
        }
        return $this->storage['file_folderbrand'];
    }

    public function setFileFolderbrand(string $fileFolderbrand): void
    {
        $this->storage['file_folderbrand'] = $fileFolderbrand;
    }

    public function getFileCategoryId(): int
    {
        if (!isset($this->storage['file_category_id'])) {
            return 0;
        }
        return intval($this->storage['file_category_id']);
    }

    public function setFileCategoryId(int $fileCategoryId): void
    {
        $this->storage['file_category_id'] = $fileCategoryId;
    }

    /**
     * @return array [name => '', type => '', tmp_name => '', size => 0]
     */
    public function getFileinput(): array
    {
        if (!isset($this->storage['file'])) {
            return [];
        }
        return $this->storage['file'];
    }

    public function getFileinputName(): string
    {
        if (!isset($this->storage['file']['name'])) {
            return '';
        }
        return $this->storage['file']['name'];
    }

    public function getFileinputType(): string
    {
        if (!isset($this->storage['file']['type'])) {
            return '';
        }
        return $this->storage['file']['type'];
    }

    public function getFileinputTmpName(): string
    {
        if (!isset($this->storage['file']['tmp_name'])) {
            return '';
        }
        return $this->storage['file']['tmp_name'];
    }

    public function getFileinputError(): int
    {
        if (!isset($this->storage['file']['error'])) {
            return 0;
        }
        return $this->storage['file']['error'];
    }

    public function getFileinputSize(): int
    {
        if (!isset($this->storage['file']['size'])) {
            return 0;
        }
        return $this->storage['file']['size'];
    }
}
